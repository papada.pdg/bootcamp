package com.scb.testAndroid.basic_interface

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.scb.testAndroid.todo.InClassIntroductionInterface
import kotlinx.android.synthetic.main.activity_main.*

abstract class AbsActivity : AppCompatActivity(), InClassIntroductionInterface {
    abstract fun navigateNext()
    abstract fun getChildLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getChildLayout())
        setView()
    }

    override fun setContent(contentText: String) {
        introductionContent.text = contentText
    }

    private fun setView(){
        setContent("Hello User, Welcome to SCB")
        btnNext.setOnClickListener {
            navigateNext()
        }
    }
}
