package com.scb.testAndroid.todo

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.scb.testAndroid.R
import com.scb.testAndroid.todo.model.TodoListModel

interface TodoClickListener{
    fun onItemClick(taskName: String)
}

class TodoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false))
{
    private val taskName: TextView = itemView.findViewById(R.id.task)
    private val checkBox: CheckBox = itemView.findViewById(R.id.checkbox)

    fun bind(model: TodoListModel, listener: TodoClickListener){
        taskName.text = model.taskName
        this.itemView.setOnClickListener{
            listener.onItemClick(taskName.text.toString())
        }
        this.checkBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
                println("pang -> $isChecked")
            }
        })

//        itemView.visibility = if (model.isComplete) {
//            View.GONE
//        } else {
//            View.VISIBLE
//        }
    }

}
