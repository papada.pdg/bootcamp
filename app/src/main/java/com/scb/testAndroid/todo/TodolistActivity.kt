package com.scb.testAndroid.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.scb.testAndroid.R
import com.scb.testAndroid.todo.model.TodoListModel
import kotlinx.android.synthetic.main.activity_todolist.*

class TodolistActivity : AppCompatActivity() {

    private lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todolist)
        setView()
    }

    private fun setView() {
        val listener = object : TodoClickListener {
            override fun onItemClick(taskName: String) {
                MainActivity.startActivity(this@TodolistActivity, taskName)
            }

        }
        todoAdapter = TodoAdapter(listener)
        rvTodo.adapter = todoAdapter
        //set linearLayout to recyclerview
        rvTodo.layoutManager = LinearLayoutManager(this)
        rvTodo.itemAnimator = DefaultItemAnimator()

        btnAdd.setOnClickListener {
            todoAdapter.addListTask(getAddModel(etInputTask.text.toString()))
            etInputTask.text = null
//            val todoListModel = TodoListModel(false, "TodoList")
//            MainActivity.startActivity(this, etInputTask.text.toString(), todoListModel)
        }
    }

    private fun getAddModel(name: String): TodoListModel{
        return TodoListModel(false, name)
    }
}
