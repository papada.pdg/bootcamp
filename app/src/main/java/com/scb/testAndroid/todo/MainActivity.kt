package com.scb.testAndroid.todo

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.scb.testAndroid.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), InClassIntroductionInterface {

    companion object{
        const val EXTRA_KEY_TITLE = "TITLE_NAME"
        const val EXTRA_KEY_MODEL = "MODEL_NAME"
        fun startActivity(context: Context, titleName: String) =
                context.startActivity(
                    Intent(context, MainActivity::class.java).also { myIntent ->
                        myIntent.putExtra(EXTRA_KEY_TITLE, titleName)
//                        myIntent.putExtra(EXTRA_KEY_MODEL, model)
                    }
                )
    }

    private val presenter = InClassIntroductionPresenter(this)

    override fun setContent(contentText: String) {
        introductionContent.text = contentText
    }

    override fun navigateToNextPage() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.init()
        this.title = intent.getStringExtra(EXTRA_KEY_TITLE)
//        val model: TodoListModel = intent.getParcelableExtra(EXTRA_KEY_MODEL)
    }

}

interface InClassIntroductionInterface {
    fun setContent(contentText: String)

    fun navigateToNextPage()
}

class InClassIntroductionPresenter(private val view: InClassIntroductionInterface){

    fun init(){
        val information = "Hello User, Welcome to SCB"
        view.setContent(information)
        view.navigateToNextPage()
    }
}
