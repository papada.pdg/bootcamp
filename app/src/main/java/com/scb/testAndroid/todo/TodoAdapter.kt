package com.scb.testAndroid.todo

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.scb.testAndroid.todo.model.TodoListModel

class TodoAdapter(private val listener: TodoClickListener) : RecyclerView.Adapter<TodoListViewHolder>() {

    private val todoList: ArrayList<TodoListModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TodoListViewHolder(parent)

    override fun getItemCount() = todoList.count()

    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
    }

    fun addListTask(taskModel: TodoListModel) {
        todoList.add(taskModel)
        notifyDataSetChanged()
    }
}