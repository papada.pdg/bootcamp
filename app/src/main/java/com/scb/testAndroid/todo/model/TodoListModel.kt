package com.scb.testAndroid.todo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//add annotation -> ทำให้ไม่ต้อง implement parcelable เยอะๆได้
@Parcelize
data class TodoListModel
    (var isComplete: Boolean,
     var taskName: String) : Parcelable