package com.scb.testAndroid.basic_api.data

import com.google.gson.annotations.SerializedName

data class BeerModel(
    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("image_url")
    val imageUrl: String?,

    @SerializedName("abv")
    val abv: Double?
)

