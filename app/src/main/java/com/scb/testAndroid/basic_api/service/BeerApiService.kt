package com.scb.testAndroid.basic_api.service

import com.scb.testAndroid.basic_api.data.BeerModel
import retrofit2.Call
import retrofit2.http.GET

interface BeerApiService {

    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>
}