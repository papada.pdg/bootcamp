package com.scb.testAndroid.basic_api.ui

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.scb.testAndroid.R
import com.scb.testAndroid.basic_api.data.BeerModel
import com.scb.testAndroid.basic_api.service.BeerManager
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_beer.*

class BeerActivity : AppCompatActivity(), BeerInterface{

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()
    }

    private fun setView(){
        this.title = "Beer Api"
        btnRefresh.setOnClickListener {
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel){
        beerName.text = beerItem.name
        abv.text = "abv: ${beerItem.abv}"
        description.text = beerItem.description
        Picasso.get()
            .load(beerItem.imageUrl)
            .placeholder(R.drawable.beer)
            .into(imageView)
    }

}
