package com.scb.testAndroid.basic_api.ui

import com.scb.testAndroid.basic_api.data.BeerModel

interface BeerInterface {
    fun setBeer(BeerItem: BeerModel)
}