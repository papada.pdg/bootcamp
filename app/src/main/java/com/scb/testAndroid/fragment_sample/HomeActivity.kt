package com.scb.testAndroid.fragment_sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.scb.testAndroid.R
import com.scb.testAndroid.fragment_sample.data.FragmentModel
import com.scb.testAndroid.fragment_sample.ui.main.PlaceholderFragment
import com.scb.testAndroid.fragment_sample.ui.main.ProfileFragment
import com.scb.testAndroid.fragment_sample.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()

    }

    private fun setView() {
        val tabList = listOf<FragmentModel>(
            FragmentModel("Home", PlaceholderFragment.newInstance(1)),
            FragmentModel("Profile", ProfileFragment.newInstance())
        )

        val sectionsPagerAdapter = SectionsPagerAdapter(tabList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }


}