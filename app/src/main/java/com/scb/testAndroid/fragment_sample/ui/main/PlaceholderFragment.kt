package com.scb.testAndroid.fragment_sample.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.scb.testAndroid.R
import kotlinx.android.synthetic.main.fragment_home.*

class PlaceholderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    //put in bundle -> to fragment
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    //to do view created
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.apply {
            val tabNumber = this.getInt(ARG_SECTION_NUMBER, 0)
            sectionLabel.text = "Tab number is $tabNumber"
        }
    }

}